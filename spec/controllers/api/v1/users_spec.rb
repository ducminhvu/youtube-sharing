require 'rails_helper'

RSpec.describe Api::V1::UsersController, type: :controller do
  describe '#POST /create' do
    subject {post :create, params: params}

    context 'when email and password is valid' do
      let(:params) {{user: {email: Faker::Internet.email, password: '12345678'}}}

      it { expect(subject).to have_http_status(:ok) }
      it { expect{subject}.to change { User.count }.by(1) }
    end

    context 'when email or password is invalid' do
      context 'when email is invalid' do
        let(:params) {{user: {email: 'invalid_email', password: '12345678'}}}

        it { expect(subject).to have_http_status(:unprocessable_entity) }
        it { expect{subject}.to change { User.count }.by(0) }
      end

      context 'when password is invalid' do
        let(:params) {{user: {email: Faker::Internet.email, password: 'short'}}}

        it { expect(subject).to have_http_status(:unprocessable_entity) }
        it { expect{subject}.to change { User.count }.by(0) }
      end
    end
  end
end
