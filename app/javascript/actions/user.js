import actionTypes from '../constants/actionTypes';
import history from '../helpers/history';
import { signupSentRequest, loginSentRequest } from '../services/userServices';

const userActions = {
  logout,
  signup,
  login,
  loginSuccess
};

function logout() {
  history.push('/');
  localStorage.removeItem('user');
  return { type: actionTypes.USER_LOG_OUT };
}

function signup(email, password, passwordConfirmation, errorCallback) {
  return () => {
    signupSentRequest(email, password, passwordConfirmation).then((dataResponse) => {
      history.push('/login');
    }).catch((response) => {
      errorCallback(response.error);
    })
  }
}

function login(email, password, errorCallback) {
  return (dispatch) => {
    loginSentRequest(email, password).then((dataResponse) => {
      let payload = { email: email, token: dataResponse.token }

      localStorage.setItem('user', JSON.stringify(payload));
      dispatch({type: actionTypes.USER_LOGIN_SUCCESS, payload: payload});
      history.push('/');
    }).catch((response) => {
      errorCallback(response.error);
    })
  }
}

function loginSuccess(email, token) {
  let payload = { email, token }
  return ({type: actionTypes.USER_LOGIN_SUCCESS, payload: payload});
}

export default userActions;
