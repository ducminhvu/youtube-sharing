import actionTypes from '../../constants/actionTypes';

const defaultUserState = {
  email: '',
  token: '',
  login: false
}

export default function(state = defaultUserState, action) {
  switch (action.type) {
    case actionTypes.USER_LOG_OUT: {
      return {
        ...state,
        email: '',
        token: '',
        login: false
      };
    }
    case actionTypes.USER_LOGIN_SUCCESS: {
      return {
        ...state,
        email: action.payload.email,
        token: action.payload.token,
        login: true
      }
    }
    default:
      return state;
  }
}
