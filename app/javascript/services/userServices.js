export const signupSentRequest = async function(email, password, passwordConfirmation) {
  let requestOption = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ user: {email, password, password_confirmation: passwordConfirmation}})
  }

  let response = await fetch('api/v1/signup', requestOption)
  let dataResponse = await response.json();
  if(response.ok) {
    return;
  } else {
    return Promise.reject(dataResponse);
  }
}

export const loginSentRequest = async function(email, password) {
  let requestOption = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ user: { email, password }})
  }

  let response = await fetch('api/v1/login', requestOption)
  let dataResponse = await response.json();
  if(response.ok) {
    return dataResponse;
  } else {
    return Promise.reject(dataResponse);
  }
}
