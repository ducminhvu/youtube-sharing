import React from 'react'
import { connect } from 'react-redux';

import FilterLink from './containers/FilterLink';
import userActions from '../actions/user';

class Header extends React.Component {
  renderGuestAction () {
    return (
      <ul className="navbar-nav">
        <li className="nav-item">
          <FilterLink filter='login' linkContent='Login' styleClass='nav-link' />
        </li>
        <li className="nav-item">
          <FilterLink filter='signup' linkContent='Signup' styleClass='nav-link' />
        </li>
      </ul>
    );
  }

  renderUserAction () {
    return (
      <ul className="navbar-nav">
        <li className="nav-item">
          <span className="navbar-text">Welcome {this.props.user.email}</span>
        </li>
        <li className="nav-item">
          <button
            className='btn btn-link'
            onClick={this.props.logout}
          >
            Logout
          </button>
        </li>
      </ul>
    );
  }

  renderHeaderAction () {
    if (this.props.user.login) {
      return this.renderUserAction();
    } else {
      return this.renderGuestAction();
    }
  }

  render () {
    return (
      <nav className="navbar navbar-light navbar-expand-lg" style={{ backgroundColor: "#e3f2fd" }}>
        <FilterLink filter='/' linkContent='Y-Sharing' styleClass='navbar-brand' />

        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto"></ul>

          {this.renderHeaderAction()}
        </div>
      </nav>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user
  }
}
const mapDispatchToProps = {
  logout: userActions.logout
}
export default connect(mapStateToProps, mapDispatchToProps)(Header);
