import React from 'react';
import { Provider } from 'react-redux';
import { Router, Route } from 'react-router-dom';

import configureStore from '../redux/configureStore';
import history from '../helpers/history';

import Header from './header';
import DashBoard from './dashboard';
import Login from './login';
import Signup from './signup';
import userActions from '../actions/user';

const store = configureStore();

class Index extends React.Component {
  constructor(props) {
    super(props);

    let user = JSON.parse(localStorage.getItem('user'));
    if (user && user.email && user.token) {
      store.dispatch(userActions.loginSuccess(user.email, user.token));
    }
  }

  render () {
    return (
      <Provider store={store}>
        <Router history={history}>
          <Header />

          <Route exact path='/' component={DashBoard} />
          <Route path='/login' component={Login} />
          <Route path='/signup' component={Signup} />
        </Router>
      </Provider>
    );
  }
}

export default Index;
