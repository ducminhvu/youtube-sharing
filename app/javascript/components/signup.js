import React from 'react'
import { connect } from 'react-redux';

import userActions from '../actions/user';

class Signup extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      passwordConfirmation: '',
      onSubmit: false,
      errorSubmit: []
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onSubmitError = this.onSubmitError.bind(this);
  }

  handleChange(event) {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  }

  handleSubmit(event) {
    event.preventDefault();

    this.setState({ onSubmit: true });
    const { email, password, passwordConfirmation } = this.state;
    if (email && password && passwordConfirmation) {
      this.props.signup(email, password, passwordConfirmation, this.onSubmitError);
    }
  }

  onSubmitError(error) {
    this.setState({ onSubmit: false, errorSubmit: error });
  }

  renderError() {
    if (this.state.errorSubmit.length == 0) {return;}

    return (
      <div className="alert alert-danger" role="alert">
        {this.state.errorSubmit.map((error, index) => <div key={index}>{error}</div>)}
      </div>
    )
  }

  render () {
    return (
      <div
        style={{
          maxWidth: 400,
          width: "100%",
          position: "fixed",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)"
        }}
      >
        <h1 className="h3 mb-3 font-weight-normal">Create your account:</h1>

        {this.renderError()}

        <form onSubmit={this.handleSubmit}>
          <input
            type="email" className="form-control" placeholder="Email address" name="email"
            value={this.state.email} onChange={this.handleChange} required
          />
          <input
            type="password" className="form-control" placeholder="Password" name="password"
            value={this.state.password} onChange={this.handleChange} required
          />
          <input
            type="password" className="form-control" placeholder="Password Confirmation"
            name="passwordConfirmation"
            value={this.state.passwordConfirmation} onChange={this.handleChange} required
          />
          <button
            className="btn btn-lg btn-primary btn-block"
            style={{ marginTop: "1em" }}
            disabled={this.state.onSubmit}
          >
            Sign up
          </button>
        </form>
      </div>
    );
  }
}

const mapDispatchToProps = {
  signup: userActions.signup
}

export default connect(null, mapDispatchToProps)(Signup);
