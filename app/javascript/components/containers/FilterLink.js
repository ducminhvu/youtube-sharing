import React from 'react'
import { NavLink } from 'react-router-dom'
import PropTypes from 'prop-types'

export default class FilterLink extends React.Component {
  render () {
    return (
      <NavLink
        exact
        to={this.props.filter}
        className={this.props.styleClass}
      >
        {this.props.linkContent}
      </NavLink>
    );
  }
}

FilterLink.propTypes = {
  filter: PropTypes.string.isRequired,
  linkContent: PropTypes.string.isRequired,
  styleClass: PropTypes.string
}
