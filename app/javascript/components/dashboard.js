import React from 'react'
import { connect } from 'react-redux';

class Dashboard extends React.Component {
  renderPromoText() {
    if (this.props.user.login) {
      return 'Please upgrade your account to use video sharing feature!'
    } else {
      return 'Please login to use video sharing feature!'
    }
  }

  render () {
    return (
      <div
        style={{
          maxWidth: 600,
          width: "100%",
          position: "fixed",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)"
        }}
      >
        <h3>{this.renderPromoText()}</h3>
        <p>
          Youtube Sharing video application..<br />
          Framework: Ruby on rails, React, Redux, Rspec,..<br />
          Server deploy info: Nginx, Puma, Capistrano, Mysql, Google Cloud Platform,..<br />
          Author: Vu Minh Duc
        </p>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user
  }
}

export default connect(mapStateToProps)(Dashboard);
