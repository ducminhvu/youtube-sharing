class User < ApplicationRecord
  has_secure_token

  devise :database_authenticatable, :registerable, :validatable
end
