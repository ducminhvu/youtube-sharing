class Api::V1::UsersController < Api::V1::ApiController
  def create
    user = User.new user_params
    if user.save
      render json: {}, status: :ok
    else
      render json: {error: user.errors.to_a}, status: :unprocessable_entity
    end
  end

  def login
    user = User.find_by email: params[:user][:email]
    if user && user.valid_password?(params[:user][:password])
      render json: {login: true, token: user.token}, status: :ok
    else
      render json: {error: ['Invalid email or password!']}, status: :unauthorized
    end
  end

  private
  def user_params
    params.require(:user).permit :email, :password, :password_confirmation
  end
end
