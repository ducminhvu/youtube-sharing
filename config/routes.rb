Rails.application.routes.draw do
  namespace :api do
    namespace :v1, constraints: { format: 'json' } do
      post 'signup', to: 'users#create'
      post 'login', to: 'users#login'
    end
  end

  root 'dashboards#index'
  get '*path', to: 'dashboards#index'
end
